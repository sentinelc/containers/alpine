This repository hosts the alpine linux base image used by official SentinelC container images.

See https://gitlab.com/sentinelc/containers/alpine/container_registry

This basically pulls alpine image from docker hub and push to gitlab registry. The goal is
to have control over the published image.
